#!/usr/bin/python2.7


from multiprocessing import Pool

# This code is expected to be runned by python2.

answer = None

# Global variable for a Pool of threats
pool = None

def checker(args):
    print("Checker!")
    if args is not None:
        answer = args
        print("Found answer:" +  str(args))
        pool.terminate()

def xor_strings(a,b):
    if len(a) != len(b):
        raise("Unable to xor string of differen length.")
    out = []
    for i in range(len(a)):
        out.append(ord(a[i]) ^ ord(b[i]))
    
    return out

def calc_range(arg):
    b = arg[0]
    e = arg[1] 
    w_list = arg[2]


    out = None
    for i in range(b, e):
        print(i)
        for j in range(i + 1, len(w_list)):
            #print("i:" + str(i) + " j:" + str(j))
            if w_xor == xor_strings(w_list[i], w_list[j]):
                out = (w_list[i], w_list[j])
                return out
    return out

if __name__ == "__main__":
    
    # This variable are expected to store answer

    c1="4A5C45492449552A"
    c2="5A47534D35525F20"
    
    print("C1:" + c1)
    print("C2:" + c2)
    
    # if you are using python 3 try somethign like this
    #val = bytes.fromhex('4a4b4c').decode('utf-8')
    wlen = len(c1.decode("hex"))
    print("Word length " + str(wlen))
    w_file = open("/usr/share/dict/words")


    w_xor = xor_strings(c1.decode("hex"), c2.decode("hex"))

    print(w_xor)

    w_list = list()
    for word in w_file:
        word = word.strip()
        if len(word) == wlen:
            w_list.append(word)
    print(len(w_list))

    pool = Pool(10)
    pool_args = [ [i*1000, (i+1)*1000 , w_list] for i in range(len(w_list)/1000)]

    res = pool.map_async(calc_range, pool_args, callback=checker)
    res.wait()

